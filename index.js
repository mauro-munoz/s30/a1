const express = require('express');
const mongoose = require('mongoose');
const app = express();
const dotenv = require('dotenv')
dotenv.config();
const port = 3000;
//Middleware used to parse data from client
app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {console.log(`Connected to Database`)
  // Connected to Database!
});

//Schema
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

//Model

const userModel = mongoose.model('userModel',userSchema)

app.post(`/signup`,(req,res)=>{

    userModel.findOne({username:req.body.username}).then((result,err)=>{
        console.log(result)
        if(result != null && result.username == req.body.username){
            res.send(`User ${result.username} is already in database`)
        }else{
            let newUser = new userModel({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save().then((result,err)=>{

                if(result){
                    res.send(`User is added to database ${result}`)
                }else{
                    res.send(`error`)
                }
            })
        }
    })
})

app.listen(port,()=>console.log(`Server is connected to port ${port}`))